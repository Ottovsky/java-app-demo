FROM openjdk:8-jre-alpine3.9
WORKDIR /usr/src/app
ADD target/demo-0.0.1-SNAPSHOT.jar /usr/src/app/
ENV PORT 5000
EXPOSE $PORT
CMD [ "sh", "-c", "java -Dserver.port=${PORT} -jar demo-0.0.1-SNAPSHOT.jar"]
